package com.example.foregroundservicedemo

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var serviceIntent: Intent? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        serviceIntent = Intent(this@MainActivity,MyService::class.java)

        serviceIntent!!.putExtra("msg","hey foreground service")

        startService.setOnClickListener{
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(serviceIntent)
            } else{
                startService(serviceIntent)
            }
        }

        stopService.setOnClickListener{stopService(serviceIntent)}

    }
}
