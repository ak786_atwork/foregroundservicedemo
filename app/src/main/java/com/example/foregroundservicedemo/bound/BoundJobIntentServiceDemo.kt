package com.example.foregroundservicedemo.bound

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.app.JobIntentService
import androidx.core.app.NotificationCompat
import com.example.foregroundservicedemo.R
import kotlinx.android.synthetic.main.video_service2_layout.view.*

class BoundJobIntentServiceDemo : JobIntentService() {
    override fun onHandleWork(intent: Intent) {

    }

    private var isFingerUp = false
    val CHANNEL_ID = "ForegroundServiceChannel"



    private var width: Int = 0
    private var height: Int = 0
    var mStartMode: Int = 0       // indicates how to behave if the service is killed
    var mBinder: IBinder = LocalBinder()      // interface for clients that bind
    var mAllowRebind: Boolean = false // indicates whether onRebind should be used

    var TAG = "service Lifecycle"

    private var currentUrl: String? = null
    private lateinit var mWindowManager: WindowManager
    private lateinit var frameView: View
    private lateinit var params: WindowManager.LayoutParams

    private var initialX: Int = 0
    private var initialY: Int = 0
    private var initialTouchX: Float = 0.toFloat()
    private var initialTouchY: Float = 0.toFloat()

    private var currentWindowWidth = 0
    private var currentWindowHeight = 0
    private val EXPAND_COLLAPSE_UNIT = 20

    private var callBacks : CallBacks? = null

    private var expandClickListener = View.OnClickListener {
        frameView.expand_image_view.visibility = View.INVISIBLE

        frameView.maximize.visibility = View.VISIBLE
        frameView.minimize.visibility = View.VISIBLE
        frameView.quit.visibility = View.VISIBLE

        mWindowManager.updateViewLayout(frameView, getParams(width,width))

        showLog("clicked")

    }


    override fun onCreate() {
        super.onCreate()

        frameView = LayoutInflater.from(this).inflate(R.layout.video_service2_layout, null)

        //getting windows services and adding the floating view to it
        mWindowManager = getSystemService(Context.WINDOW_SERVICE) as WindowManager

        params = getParams(400,400)
        mWindowManager.addView(frameView, params)

        displayConfig()
        setUpDrag()

        setUpWebView()

        addControls()
    }

    fun setCallback(callBacks: CallBacks){
        this.callBacks = callBacks
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val input = intent!!.getStringExtra("msg")
        createNotificationChannel()
        val notificationIntent = Intent(this, JobActivity::class.java)
        val pendingIntent = PendingIntent.getActivity(
            this,
            0, notificationIntent, 0
        )

        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("Foreground Service")
            .setContentText(input)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setStyle(NotificationCompat.DecoratedCustomViewStyle())
            .setContentIntent(pendingIntent)
            .build()

        startForeground(1, notification)

        return Service.START_NOT_STICKY
    }


    private fun addControls() {
        frameView.maximize.setOnClickListener{
            mWindowManager.updateViewLayout(frameView,getParams(width,width))
        }

        frameView.quit.setOnClickListener{ callBacks?.stopService()}

        frameView.minimize.setOnClickListener{

            resetVars()

            mWindowManager.updateViewLayout(frameView, params)
            frameView.expand_image_view.visibility = View.VISIBLE
            frameView.maximize.visibility = View.INVISIBLE
            frameView.minimize.visibility = View.INVISIBLE
            frameView.quit.visibility = View.INVISIBLE
        }

  /*      frameView.expand_image_view.setOnLongClickListener {
            frameView.expand_image_view.visibility = View.INVISIBLE
            mWindowManager.updateViewLayout(frameView,getParams(width))
            return@setOnLongClickListener true

        }*/


     /*   frameView.expand_image_view.setOnClickListener {
//            if (isFingerUp) {
                frameView.expand_image_view.visibility = View.INVISIBLE
                mWindowManager.updateViewLayout(frameView, getParams(width,width))
//            }
        }*/

        frameView.expand_image_view.setOnClickListener(expandClickListener)


        frameView.resize_mode.setOnClickListener{
            toggleVisibility(frameView.zoom_control_layout)
        }

        //collapse horizontal
        frameView.zoom_control_layout.collapse_horizontal.setOnClickListener{
        }

        //expand horizontal
        frameView.zoom_control_layout.expand_horizotal.setOnClickListener{

        }

        //collapse vertical
        frameView.zoom_control_layout.collapse_vertical.setOnClickListener{

        }


        //expand vertical
        frameView.zoom_control_layout.expand_vertical.setOnClickListener{

        }




    }

    private fun toggleVisibility(view: View) {
        view.visibility = if(view.visibility == View.VISIBLE )  View.INVISIBLE else View.VISIBLE
    }


    private fun setUpWebView() {
        frameView.webView.loadUrl("https://www.youtube.com/")


        frameView.webView.settings.javaScriptEnabled = true
        frameView.webView.settings.builtInZoomControls = true

        val newUA = "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0"
        frameView.webView.settings.userAgentString = newUA


        frameView.webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
//                showLog(url)
                currentUrl = url
                url?.let { loadSong(it) }
                return true
            }
        }
    }

    fun loadSong(url: String) {
        frameView.webView.loadUrl(url)
    }

    private fun resetVars(){
//        initialTouchX = 200f
//        initialTouchY = 80f

        initialX = 0
        initialY = 0

        var p = getParams(200,80)
        params.x = p.x
        params.y = p.y
        params.width = 150
        params.height = 100
    }

    private fun displayConfig() {
        val displayMetrics = DisplayMetrics()
        mWindowManager.getDefaultDisplay().getMetrics(displayMetrics)
        height = displayMetrics.heightPixels
        width = displayMetrics.widthPixels

        showLog("width = $width height = $height")
    }

    private fun setUpDrag() {
        //adding an touchlistener to make drag movement of the floating widget
        frameView.setOnTouchListener(object : View.OnTouchListener {


            override fun onTouch(v: View, event: MotionEvent): Boolean {
                showLog("x = ${event.x} y = ${event.y}")

                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        initialX = params.x
                        initialY = params.y
                        initialTouchX = event.rawX
                        initialTouchY = event.rawY
                        return true
                    }

                    MotionEvent.ACTION_UP ->
                        //when the drag is ended switching the state of the widget
                        //                        collapsedView.setVisibility(View.GONE);
                        //                        expandedView.setVisibility(View.VISIBLE);
                        return true

                    MotionEvent.ACTION_MOVE -> {
                        //this code is helping the widget to move around the screen with fingers
                        params.x = initialX + (event.rawX - initialTouchX).toInt()
                        params.y = initialY + (event.rawY - initialTouchY).toInt()
                        mWindowManager.updateViewLayout(frameView, params)
                        return true
                    }
                }
                return false
            }
        })

        //adding an touchlistener to make drag movement of the floating widget
/*        frameView.expand_image_view.setOnTouchListener(object : View.OnTouchListener {


            override fun onTouch(v: View, event: MotionEvent): Boolean {
                showLog("x = ${event.x} y = ${event.y}")

                isFingerUp = false
                when (event.action) {
                    MotionEvent.ACTION_DOWN -> {
                        initialX = params.x
                        initialY = params.y
                        initialTouchX = event.rawX
                        initialTouchY = event.rawY
                        return true
                    }

                    MotionEvent.ACTION_UP -> {
                        //when the drag is ended switching the state of the widget
                        //                        collapsedView.setVisibility(View.GONE);
                        //                        expandedView.setVisibility(View.VISIBLE);

                        isFingerUp = true
                        return true
                    }

                    MotionEvent.ACTION_MOVE -> {
                        //this code is helping the widget to move around the screen with fingers
                        params.x = initialX + (event.rawX - initialTouchX).toInt()
                        params.y = initialY + (event.rawY - initialTouchY).toInt()
                        mWindowManager.updateViewLayout(frameView, params)

                        return true
                    }
                }
                return false
            }
        })*/
    }


    private fun getParams(w: Int,h : Int): WindowManager.LayoutParams {
        val LAYOUT_FLAG: Int
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY
        } else {
            LAYOUT_FLAG = WindowManager.LayoutParams.TYPE_PHONE
        }
        //setting the layout parameters
        return WindowManager.LayoutParams(
            w,
            w,
            LAYOUT_FLAG,
            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
            PixelFormat.TRANSLUCENT
        )
    }


    override fun onDestroy() {
        super.onDestroy()
        if (frameView != null) mWindowManager.removeView(frameView)
    }


    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    inner class LocalBinder : Binder() {
        internal// Return this instance of LocalService so clients can call public methods
        val service: BoundJobIntentServiceDemo
            get() = this@BoundJobIntentServiceDemo
    }



    override fun onBind(intent: Intent): IBinder? {
        // A client is binding to the service with bindService()
        Log.d(TAG, "onBind called")
        return mBinder
    }

    override fun onUnbind(intent: Intent): Boolean {
        // All clients have unbound with unbindService()
        Log.d(TAG, "onUnbind called")
        return true
    }

    override fun onRebind(intent: Intent) {
        // A client is binding to the service with bindService(),
        // after onUnbind() has already been called
        Log.d(TAG, "onRebind called")
    }

    private fun showLog(url: String?) {
        Log.d("VideoService", url)
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(
                CHANNEL_ID,
                "Foreground Service Channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )

            val manager = getSystemService(NotificationManager::class.java)
            manager!!.createNotificationChannel(serviceChannel)
        }
    }

    interface CallBacks{
        fun stopService()
    }

}