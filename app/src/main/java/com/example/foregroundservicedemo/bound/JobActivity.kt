package com.example.foregroundservicedemo.bound

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.provider.Settings
import android.util.Log
import android.view.GestureDetector
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.Toast
import com.example.foregroundservicedemo.R
import kotlinx.android.synthetic.main.activity_job.*

class JobActivity : AppCompatActivity(), View.OnClickListener{


    private  var videoService: BoundJobIntentServiceDemo? = null
    private val SYSTEM_ALERT_WINDOW_PERMISSION = 2084
    private var serviceIntent: Intent? = null
    private var currentUrl: String? = null
    private var isServiceStarted = false


    private val serviceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            //get the instance and call public method
            val binder = service as BoundJobIntentServiceDemo.LocalBinder
            videoService = binder.service
            currentUrl?.let { videoService!!.loadSong(it) }
            videoService!!.setCallback(object : BoundJobIntentServiceDemo.CallBacks{
                override fun stopService() {
                    onClick(removeWidget)
                }
            })

        }

        override fun onServiceDisconnected(name: ComponentName) {

        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_job)

        serviceIntent = Intent(this@JobActivity, BoundJobIntentServiceDemo::class.java)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Settings.canDrawOverlays(this)) {
            askPermission()
        }

        serviceIntent = Intent(this@JobActivity, BoundJobIntentServiceDemo::class.java)


        buttonCreateWidget.setOnClickListener(this)
        removeWidget.setOnClickListener(this)
        setUpWebView()

    }

    private fun setUpWebView() {
        webView.loadUrl("https://www.youtube.com/")

        webView.settings.javaScriptEnabled = true

        val newUA = "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0"
        webView.settings.userAgentString = newUA


        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                showLog(url)
                currentUrl = url


                if (url != null) {
                    if (url.contains("query")) {
                        webView.loadUrl(url)
                    } else{
                        if(!isServiceStarted)
                            onClick(buttonCreateWidget)
                        videoService?.loadSong(url)
                    }
                }
                return true
            }
        }
    }

    private fun showLog(url: String?) {
        Log.d("VideoActivity", url)
    }


    private fun askPermission() {
        val intent = Intent(
            Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
            Uri.parse("package:$packageName")
        )
        startActivityForResult(intent, SYSTEM_ALERT_WINDOW_PERMISSION)
    }

    override fun onClick(v: View) {
        if (v.id == R.id.removeWidget) {
            isServiceStarted = false
            unbindService(serviceConnection)
            stopService(serviceIntent)
        } else {
            isServiceStarted = true
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                bindService(serviceIntent,serviceConnection, Context.BIND_AUTO_CREATE)
                startService(serviceIntent)
            } else if (Settings.canDrawOverlays(this)) {
                bindService(serviceIntent,serviceConnection, Context.BIND_AUTO_CREATE)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(serviceIntent)
                } else
                    startService(serviceIntent)
            } else {
                askPermission()
                Toast.makeText(this, "You need System Alert Window Permission to do this", Toast.LENGTH_SHORT).show()
            }
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        if (isServiceStarted && serviceIntent != null)
            stopService(serviceIntent)
    }

}
